package txscript
import (
	"github.com/decred/dcrd/dcrec/secp256k1/v3"
	"github.com/decred/dcrd/dcrec/secp256k1/v3/ecdsa"
)
type Signable interface {
	GetPubKey() *secp256k1.PublicKey
	Sign(hash []byte) (*ecdsa.Signature, error)
}