package txscript

import (
	"github.com/decred/dcrd/dcrec/secp256k1/v3"
	"github.com/decred/dcrd/dcrec/secp256k1/v3/ecdsa"
)

type PrivateKeySignable struct {
	privateKey *secp256k1.PrivateKey
}

// NewPrivateKeySignable create a new instance of PrivateKeySignable
func NewPrivateKeySignable(priKey *secp256k1.PrivateKey) *PrivateKeySignable {
	return &PrivateKeySignable{privateKey: priKey}
}

// Sign the given hash bytes
func (p *PrivateKeySignable) Sign(hash []byte) (*ecdsa.Signature, error) {
	return ecdsa.Sign(p.privateKey, hash), nil // TODO: is it possible for Sign() to fail?
}

// GetPubKey return the PubKey
func (p *PrivateKeySignable) GetPubKey() *secp256k1.PublicKey {
	return p.privateKey.PubKey()
}
